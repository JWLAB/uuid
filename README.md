# uuid
----
    import "gitlab.com/JWLAB/uuid"

Package uuid provides a method to generate a uuid v4 compliant string.

## Usage

#### func  New

```go
func New() (string, error)
```
New generates a single v4 compliant uuid. It returns the uuid as a string if no
errors occur.
