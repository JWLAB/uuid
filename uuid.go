//Package uuid provides a method to generate a uuid v4 compliant string.
package uuid

import (
	"crypto/rand"
	"fmt"
	"io"
)

var randByte = rand.Reader

//New generates a single v4 compliant uuid.
//It returns the uuid as a string if no errors occur.
func New() (string, error) {
	b := make([]byte, 16)
	_, err := io.ReadFull(randByte, b)
	if err != nil {
		//This should never happen.
		//Return empty string since no UUID can be generated with error.
		return "", err
	}
	//Zero out and set the value of the first 4 bits of the sixth byte to hex "4".
	b[6] = (b[6] & 0x0f) | 0x40
	//Ensure the first 4 bits of the eighth are one of the hex values "a", "b", "9", "8".
	b[8] = (b[8] & 0x3f) | 0x80
	return fmt.Sprintf("%8x-%4x-%4x-%4x-%12x", b[:4], b[4:6], b[6:8], b[8:10], b[10:]), nil
}
