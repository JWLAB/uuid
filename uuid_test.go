package uuid

import (
	"fmt"
	"regexp"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_New(t *testing.T) {
	Convey("Given new UUID is generated.", t, func() {
		uuid, _ := New()
		match, _ := regexp.MatchString("^[0-9a-f]{8}-[0-9a-f]{4}-[4][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$", uuid)
		fmt.Println(uuid)
		Convey("A valid v4 UUID is generated.", func() {
			So(match, ShouldBeTrue)
		})
	})
}

func Test_NewUnique(t *testing.T) {
	Convey("Given 100000 UUIDs are generated.", t, func() {
		umap := make(map[string]bool)
		for i := 0; i < 100000; i++ {
			id, _ := New()
			umap[id] = true
		}
		Convey("Then 100000 valid v4 UUIDs are generated.", func() {
			So(len(umap), ShouldEqual, 100000)
		})
	})
}

var result string

func Benchmark_New(b *testing.B) {
	var uuid string
	for n := 0; n < b.N; n++ {
		uuid, _ = New()
	}
	result = uuid
}
